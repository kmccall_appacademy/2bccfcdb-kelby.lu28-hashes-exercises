# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  word_freq = Hash.new(0)
  str.split(" ").each do |word|
    word_freq[word] = word.length
  end

  word_freq
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by { |k, v| v }[-1][0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |key, value|
    older[key] = newer[key]
  end

  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  letter_freq = Hash.new(0)

  word.each_char do |letter|
    letter_freq[letter] += 1
  end

  letter_freq
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  elements = Hash.new(0)

  arr.each { |ele| elements[ele] += 1 }
  elements.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  parities = Hash.new(0)

  numbers.each do |num|
    num.even? ? parities[:even] += 1 : parities[:odd] += 1
  end

  parities
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = most_frequent_vowels(vowel_frequency(string))

  vowels.keys.sort[0]
end

def vowel_frequency(string)
  vowels = "aeiou"
  vowel_hash = Hash.new(0)

  string.each_char do |char|
    vowel_hash[char] += 1 if vowels.include?(char.downcase)
  end

  vowel_hash
end

def most_frequent_vowels(vowel_hash)
  vowel_hash.select { |key, value| value == vowel_hash.values.max }
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  names = later_birthday_names(students)
  names.combination(2)
end

def later_birthday_names(students)
  students.select { |names, months| names if months > 6 }.keys
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  no_species(specimens)**2 * (min_pop_species(specimens) /
  max_pop_species(specimens))
end

def specimen_hash(specimens)
  spec_frequency = Hash.new(0)

  specimens.each do |spec|
    spec_frequency[spec] += 1
  end

  spec_frequency
end

def no_species(specimens)
  specimen_hash(specimens).count
end

def min_pop_species(specimens)
  specimen_hash(specimens).values.sort.first
end

def max_pop_species(specimens)
  specimen_hash(specimens).values.sort.last
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  vandal_hash = character_count(vandalized_sign)
  normal_hash = character_count(normal_sign)

  vandal_hash.each do |letter, count|
    return false if count > normal_hash[letter]
  end

  true
end

def character_count(str)
  char_frequency = Hash.new(0)

  str.each_char do |char|
    char_frequency[char.downcase] += 1 unless char == " "
  end

  char_frequency
end
